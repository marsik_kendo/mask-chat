package com.mask.client;

import javafx.application.Platform;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Network {

    private static Socket socket;

    private static DataInputStream in;
    private static DataOutputStream out;

    public static Callback onCloseConnection;

    public static Callback onAuthenticated;

    public static Callback onMsgReceived;


    private static void connect(){
        try {
            socket = new Socket("localhost", 8189);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            Thread thread = new Thread(() -> {
                try {
                    while (true){
                        String msg = in.readUTF();
                        if(msg.equals("/authok")){
                            onAuthenticated.callback();
                            break;
                        }
                    }
                    while (true) {
                        String msg = in.readUTF();
                        if(msg.equals("/exit")){
                            break;
                        }
                        onMsgReceived.callback(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    closeConnection();
                }
            });
            thread.setDaemon(true);
            thread.start();


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static boolean sendMsg(String msg){
        try {
            out.writeUTF(msg);
            //alert
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean sendAuth(String login, String pass){

        try {
            if(socket == null || socket.isClosed()){
                connect();
            }
            out.writeUTF("/auth " +login + " " + pass);
           return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void closeConnection() {
        onCloseConnection.callback();
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
