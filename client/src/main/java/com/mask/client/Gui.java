package com.mask.client;

import com.sun.javafx.scene.control.FakeFocusTextField;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Gui extends Application {
    public static Stage stage = null;

    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(Gui.class.getResource("/sample.fxml"));
        stage.setTitle("Chat");
        stage.setScene(new Scene(root, 400, 400));
        stage.show();
        Gui.stage = stage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
