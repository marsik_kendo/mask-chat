package com.mask.client;


import java.io.*;
import java.net.Socket;

import java.util.Scanner;

public class SimpleClient {

    static DataOutputStream out = null;
    static DataInputStream in = null;

    public static void main(String[] args) {

        Socket socket = null;

        try {
            socket = new Socket("127.0.0.1", 8189);

            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());

            System.out.println("Пишите соосбщение");
            Scanner scanner = new Scanner(System.in);
            new Thread(() -> {
                while (true) {
                    try {
                        String msg = in.readUTF();
                        System.out.println(msg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            while (true) {
                String msg = scanner.nextLine();
                out.writeUTF(msg);
            }

        } catch (IOException e) {
            disconnect(e, in, out, socket);
        }
    }

    private static void disconnect(IOException e, DataInputStream in, DataOutputStream out, Socket socket) {
        try {
            in.close();
        } catch (IOException ex) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException ex) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException ex) {
            e.printStackTrace();
        }
    }

}
