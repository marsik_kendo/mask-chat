package com.mask.client;


@FunctionalInterface
public interface Callback {

    void callback(Object... args);
}
