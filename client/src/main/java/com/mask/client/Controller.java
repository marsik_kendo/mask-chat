package com.mask.client;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;


public class Controller implements Initializable{



    @FXML
    TextField textField, loginField;

    @FXML
    TextArea textArea;

    @FXML
    PasswordField passField;

    @FXML
    Button sendButton;

    @FXML
    HBox authPanel, sendPanel;

    @FXML
    ListView<String> clientsList;

    private boolean authentificated;

    private void setAuthentificated(boolean authentificated) {
        this.authentificated = authentificated;
        authPanel.setVisible(!authentificated);
        authPanel.setManaged(!authentificated);
        sendPanel.setVisible(authentificated);
        sendPanel.setManaged(authentificated);
        clientsList.setVisible(authentificated);
        clientsList.setManaged(authentificated);
    }

    public void sendMsg(){
        if (Network.sendMsg(textField.getText())) {
            textField.clear();
        }
    }

    public void sendAuth(){
        if (Network.sendAuth(loginField.getText(), passField.getText())) {
            loginField.clear();
            passField.clear();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setAuthentificated(false);
        linkCallbacks();
        clientsList.setOnMouseClicked(mouseEvent -> {

            if (mouseEvent.getClickCount() == 2) {
                String nickname = clientsList.getSelectionModel().getSelectedItem();
                textField.setText("/w " + nickname + " ");
                textField.requestFocus();
                textField.selectEnd();

            }

        });
    }

    public void linkCallbacks(){
        Network.onCloseConnection = args -> setAuthentificated(false);
        Network.onAuthenticated = args -> setAuthentificated(true);
        Network.onMsgReceived = args -> {
            String msg = args[0].toString();
            if(msg.startsWith("/")){
                 if(msg.startsWith("/clients ")){
                    String[] tokens = msg.split("\\s");
                    Platform.runLater(() -> {
                        clientsList.getItems().clear();
                        for (int i = 1; i < tokens.length; i++) {
                            clientsList.getItems().add(tokens[i]);
                        }
                    });
                }
            } else {
                textArea.appendText(msg + "\n");
            }
        };

    }
}
