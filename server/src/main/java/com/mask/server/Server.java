package com.mask.server;

import com.mask.server.service.AuthService;
import com.mask.server.service.ClientHandler;
import com.mask.server.service.SimpleAuthService;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private List<ClientHandler> clients = new ArrayList<>();

    private AuthService authService;

    public AuthService getAuthService() {
        return authService;
    }

    public Server() {
        this.authService = new SimpleAuthService();
        try (ServerSocket serverSocket = new ServerSocket(8189)) {

            System.out.println("Сервер запустился");

            while (true){
                Socket socket = serverSocket.accept();
                System.out.println("Подключился новый клиент");

               new ClientHandler(socket, this);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Сервер выключается...");
    }

    public void broadcastMessage(String msg){
        for (ClientHandler client : clients){
            client.sendMsg(msg);
        }
    }

    public boolean nicknameIsBusy(String nickname){

       return clients.stream().anyMatch(client ->  client.getNickname().equals(nickname));
    }

    public void subscribe(ClientHandler clientHandler){
        clients.add(clientHandler);
        broadcastClientsList();
    }
    public void unSubscribe(ClientHandler clientHandler){
        clients.remove(clientHandler);
        broadcastClientsList();
    }


    public void broadcastClientsList(){
        // /clients nick1 nick2 nick3
        StringBuilder sb = new StringBuilder(6 * clients.size());
        sb.append("/clients ");

        for (ClientHandler client : clients){
           sb.append(client.getNickname()).append(" ");
        }
        sb.setLength(sb.length() - 1);
        String out = sb.toString();
        broadcastMessage(out);
    }
}
