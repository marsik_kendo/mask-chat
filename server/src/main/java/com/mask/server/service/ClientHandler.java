package com.mask.server.service;

import com.mask.server.Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.stream.Stream;

public class ClientHandler {
    DataInputStream in;
    DataOutputStream out;

    String nickname;

    private Server server;
    private Socket socket;

    public String getNickname() {
        return nickname;
    }

    public ClientHandler(Socket socket, Server server) {
        try {
            this.socket = socket;
            this.server = server;
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            Thread thread = new Thread(() -> {
                try {
                    while (true) {
                        String msg = in.readUTF();
                        if(msg.startsWith("/auth ")){
                            // /auth login1 pass1
                            String[] tokens = msg.split("\\s");
                            String login = tokens[1];
                            String pass = tokens[2];

                            String nickname = server.getAuthService().getNickNameByLoginAndPassword(login, pass);
                            if(nickname != null && !server.nicknameIsBusy(nickname)) {
                                this.nickname = nickname;
                                sendMsg("/authok");
                                server.subscribe(this);
                                break;
                            }
                        }
                    }

                    while (true) {
                        String msg = readMsg();
                        if (msg.equals("/end")) {
                            break;
                        } else if(msg.equals("/exit")){
                            sendMsg("/exit");
                            break;
                        } else {
                            server.broadcastMessage(nickname + ": " + msg);
                        }
                    }
                } catch (Exception e){
                     e.printStackTrace();
                }
                finally {
                    disconnect();
                }
            });
            thread.setDaemon(true);
            thread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void disconnect(){
        server.unSubscribe(this);
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private String readMsg()  {
        try {
            return in.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void sendMsg(String msg) {
        try {
            out.writeUTF(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
