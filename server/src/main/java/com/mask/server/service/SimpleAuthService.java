package com.mask.server.service;

import com.mask.server.model.UserData;
import com.mask.server.service.AuthService;

import java.util.ArrayList;
import java.util.List;

public class SimpleAuthService implements AuthService {


    private List<UserData> users = new ArrayList<>();


    public SimpleAuthService() {

        for (int i = 1; i <= 10; i++) {
            users.add(new UserData("login" + i, "pass" + i, "nickname" + i));
        }
    }

    @Override
    public String getNickNameByLoginAndPassword(String login, String password) {

        return users.stream()
                .filter(user -> user.getLogin().equals(login) && user.getPass().equals(password))
                .map(UserData::getNickName)
                .findFirst()
                .orElse(null) ;
    }
}
