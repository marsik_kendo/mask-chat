package com.mask.server.service;

public interface AuthService {

    String getNickNameByLoginAndPassword(String login, String password);
}
