package com.mask.server.model;

import java.io.ByteArrayInputStream;

public class UserData {

    private String login;
    private String pass;
    private String nickName;

    private String role;

    public UserData(String login, String pass, String nickName) {
        this.login = login;
        this.pass = pass;
        this.nickName = nickName;

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
