package com.mask.server;

import java.util.Scanner;

public class Main {


    public static  DoSomething doSomething;

    public static void main(String[] args) throws InterruptedException {

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    System.out.println( Thread.currentThread().getName() + " " + scanner.nextLine());
                }
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    System.out.println(Thread.currentThread().getName() + " " + scanner.nextLine());
                }
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

    }

    public static void qwe(DoSomething doSomething){

        doSomething.doSomething();
    }

}
